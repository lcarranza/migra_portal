--Actualizando convenio a estado Finalizado
update GNR_CONVENIO_EMP_INSTALAC set estado = 'F' where ID_CONVENIO_EMP_INSTALAC in (7,23);

--ACTUALIZACION DE CONVENIOS
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('30/08/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 30;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('29/12/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 15;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('11/05/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 18;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('09/03/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 19;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('07/09/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 16;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('01/09/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 7;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('09/06/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 20;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('14/09/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 9;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('08/09/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 13;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('10/04/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 10;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('09/07/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 17;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('11/07/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 21;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('07/09/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 11;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('11/01/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 12;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('17/04/2017') WHERE ID_CONVENIO_EMP_INSTALAC = 61;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_FIN = TO_DATE('05/12/2017') WHERE ID_CONVENIO_EMP_INSTALAC = 24;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('29/12/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 25;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('29/12/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 26;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('28/12/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 28;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('29/12/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 29;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('29/12/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 22;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('04/03/2017') WHERE ID_CONVENIO_EMP_INSTALAC = 101;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('21/04/2017') WHERE ID_CONVENIO_EMP_INSTALAC = 241;
UPDATE GNR_CONVENIO_EMP_INSTALAC SET FECHA_INICIO = TO_DATE('09/03/2016') WHERE ID_CONVENIO_EMP_INSTALAC = 181;
INSERT INTO GNR_CONVENIO_EMP_INSTALAC (ID_CONVENIO_EMP_INSTALAC,ID_EMPRESA_INSTALADORA,ID_ZONA_ADJUDICACION,FECHA_INICIO,FECHA_FIN,FECHA_FIN_MODIFICADA,RESPONSABLE_LIQUID_RECAUD,PORCENTAJE_RETENCION,DIAS_ESPERA_DEVOLUC_RETENCION,MONEDA_COSTO_OTROS_MAXIMO,MONTO_COSTO_OTROS_MAXIMO,ESTADO,USUARIO_CREACION,FECHA_CREACION,TERMINAL_CREACION,USUARIO_MODIFICACION,FECHA_MODIFICACION,TERMINAL_MODIFICACION,MONEDA_EMPRESA_INSTALADORA,NU_ASIENTO,NU_PARTIDA,DE_ZONA_REGIONAL,DE_SEDE,DE_OFICINA_REGIONAL) 
VALUES (GNR_CONVENIO_EMP_INSTALAC_SEQ.NEXTVAL,25,106,to_date('11/09/2016','DD/MM/RR'),to_date('29/11/2016','DD/MM/RR'),null,null,'7','60','S','0','F','admingnr',SYSDATE,'127.0.0.1','admingnr',SYSDATE,'127.0.0.1','S',null,null,null,null,null);
INSERT INTO GNR_CONVENIO_EMP_INSTALAC (ID_CONVENIO_EMP_INSTALAC,ID_EMPRESA_INSTALADORA,ID_ZONA_ADJUDICACION,FECHA_INICIO,FECHA_FIN,FECHA_FIN_MODIFICADA,RESPONSABLE_LIQUID_RECAUD,PORCENTAJE_RETENCION,DIAS_ESPERA_DEVOLUC_RETENCION,MONEDA_COSTO_OTROS_MAXIMO,MONTO_COSTO_OTROS_MAXIMO,ESTADO,USUARIO_CREACION,FECHA_CREACION,TERMINAL_CREACION,USUARIO_MODIFICACION,FECHA_MODIFICACION,TERMINAL_MODIFICACION,MONEDA_EMPRESA_INSTALADORA,NU_ASIENTO,NU_PARTIDA,DE_ZONA_REGIONAL,DE_SEDE,DE_OFICINA_REGIONAL) 
VALUES (GNR_CONVENIO_EMP_INSTALAC_SEQ.NEXTVAL,1365,101,to_date('09/02/2016','DD/MM/RR'),to_date('16/12/2017','DD/MM/RR'),null,null,'7','60','S','0','F','admingnr',SYSDATE,'127.0.0.1','admingnr',SYSDATE,'127.0.0.1','S',null,null,null,null,null);

--Reemplazando id_empresa 26 REINTECSA por 1365 CONSORCIO REINTECSA
update gnr_solicitud s
set s.id_empresa_instaladora = 1365
where s.id_solicitud in (select ash.id_solicitud from GNR_TH_SOL_FISE ash WHERE TI_ACCION = 'A')
and s.id_empresa_instaladora = 26;

--update id_convenio match perfecto
UPDATE gnr_solicitud s
SET s.id_convenio_emp_instalac = 
    (
       select min(conv.id_convenio_emp_instalac)
        from gnr_convenio_emp_instalac conv
        inner join gnr_distrito_zona_adjudic dist
        on dist.id_zona_adjudicacion = conv.id_zona_adjudicacion
        inner join gnr_EMP_ejec_conv_emp_insT eje
        on conv.id_convenio_emp_instalac = eje.id_convenio_emp_instalac
        WHERE 1=1
        AND dist.ubigeo_distrito = s.ubigeo_predio
        and  eje.id_empresa_instaladora = s.id_empresa_instaladora
        and ( trunc(eje.fecha_inicio) <= trunc(s.FECHA_REGISTRO_PORTAL)
                and trunc(eje.fecha_fin) >= trunc(s.FECHA_REGISTRO_PORTAL))

    )
WHERE s.id_solicitud in (select ash.id_solicitud from GNR_TH_SOL_FISE ash WHERE TI_ACCION = 'A')
AND s.id_convenio_emp_instalac is null;

--update id_convenio limites alargados
UPDATE gnr_solicitud s
SET s.id_convenio_emp_instalac = 
    (
       select min(conv.id_convenio_emp_instalac)
        from gnr_convenio_emp_instalac conv
        inner join gnr_distrito_zona_adjudic dist
        on dist.id_zona_adjudicacion = conv.id_zona_adjudicacion
        inner join gnr_EMP_ejec_conv_emp_insT eje
        on conv.id_convenio_emp_instalac = eje.id_convenio_emp_instalac
        inner join (
            select 270 as ajuste from dual
        ) x on 1=1
        WHERE 1=1
        AND dist.ubigeo_distrito = s.ubigeo_predio
        and  eje.id_empresa_instaladora = s.id_empresa_instaladora
        and ( trunc(eje.fecha_inicio) <= trunc(s.FECHA_REGISTRO_PORTAL + x.ajuste)
                and trunc(eje.fecha_fin) >= trunc(s.FECHA_REGISTRO_PORTAL) + x.ajuste)
    )
WHERE s.id_solicitud in (select ash.id_solicitud from GNR_TH_SOL_FISE ash WHERE TI_ACCION = 'A')
AND s.id_convenio_emp_instalac is null;

--update id_convenio sin restriccion de fecha
UPDATE gnr_solicitud s
SET s.id_convenio_emp_instalac = 
    (
       select min(conv.id_convenio_emp_instalac)
        from gnr_convenio_emp_instalac conv
        inner join gnr_distrito_zona_adjudic dist
        on dist.id_zona_adjudicacion = conv.id_zona_adjudicacion
        inner join gnr_EMP_ejec_conv_emp_insT eje
        on conv.id_convenio_emp_instalac = eje.id_convenio_emp_instalac
        WHERE 1=1
        AND dist.ubigeo_distrito = s.ubigeo_predio
        and  eje.id_empresa_instaladora = s.id_empresa_instaladora        
    )
WHERE s.id_solicitud in (select ash.id_solicitud from GNR_TH_SOL_FISE ash WHERE TI_ACCION = 'A')
AND s.id_convenio_emp_instalac is null;

--update id_convenio sin restriccion de fecha ni distrito
UPDATE gnr_solicitud s
SET s.id_convenio_emp_instalac = 
    (
       select min(conv.id_convenio_emp_instalac)
        from gnr_convenio_emp_instalac conv
        inner join gnr_distrito_zona_adjudic dist
        on dist.id_zona_adjudicacion = conv.id_zona_adjudicacion
        WHERE substr(dist.ubigeo_distrito,0,2) = substr(s.ubigeo_predio,0,2)
        and  conv.id_empresa_instaladora = s.id_empresa_instaladora        
    )
WHERE s.id_solicitud in (select ash.id_solicitud from GNR_TH_SOL_FISE ash WHERE TI_ACCION = 'A')
AND s.id_convenio_emp_instalac is null;

commit;