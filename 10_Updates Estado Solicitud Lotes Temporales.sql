--todos aquellos registros que fueron procesados por los lotes temporales
--se les debe corregir el estado manualmente

UPDATE gnr_solicitud sol
SET sol.id_estado_solicitud = 25,
sol.id_lote_liquidac_instaladora = null
WHERE sol.id_solicitud in ( SELECT lt.id_solicitud 
                            FROM GNR_TH_SOL_FISE lt
                            WHERE lt.id_estado_solicitud = 15
                            AND LT.TI_ACCION = 'L'
                            );

UPDATE gnr_solicitud sol
SET sol.id_estado_solicitud = 23,
sol.id_lote_liquidac_instaladora = null
WHERE sol.id_solicitud in ( SELECT lt.id_solicitud 
                            FROM GNR_TH_SOL_FISE lt
                            WHERE lt.id_estado_solicitud = 27
                            AND LT.TI_ACCION = 'L'
                            );
                            
COMMIT;                            