--Desactivar los registros de habilitacion activos que tengan respuesta rechazada
update gnr_habilitacion H
SET H.estado = 'I'
WHERE H.estado = 'A'
AND NVL(H.APROBADO,'N') <> 'S'
AND H.ID_SOLICITUD IN ( SELECT ID_SOLICITUD FROM GNR_TH_SOL_FISE WHERE TI_ACCION = 'A')
;