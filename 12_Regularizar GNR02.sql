--Se regresan los estados corregidos a las solicitudes 
--que tuvieron su lote temporal
UPDATE gnr_solicitud sol
SET sol.id_estado_solicitud = 28
WHERE sol.id_solicitud in ( SELECT lt.id_solicitud 
                            FROM GNR_TH_SOL_FISE lt
                            WHERE lt.id_estado_solicitud = 15
                            AND LT.TI_ACCION = 'L'
                            );

UPDATE gnr_solicitud sol
SET sol.id_estado_solicitud = 26
WHERE sol.id_solicitud in ( SELECT lt.id_solicitud 
                            FROM GNR_TH_SOL_FISE lt
                            WHERE lt.id_estado_solicitud = 27
                            AND LT.TI_ACCION = 'L'
                            );
                            
COMMIT;                            